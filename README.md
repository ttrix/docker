# Docker module

Download docker desktop
https://www.docker.com/products/docker-desktop/

## What is a container?

A way to package applications with **all** the necessary **dependencies** and **configuration**.

Portable artifact, easily shared.

Container images are stored in private or public repositories (nexus, DockerHub ...)

#### Helping to improve application development

- running a container with all the required settings for the dev environment instead of having the need to install every thing (like binaries, components) manually what is likely for lots of humain errors.
- One command to install the app
- Run 2 versions of the same application without conflict thanks to the isolation of the containers

#### Better deployment process

Instead of sending artifacts and a set of instructions to Operations team, which could leads to misunderstandings, dependency conflicts, forget mentioning instructions/steps

Every thing is encapsulated

## Container vs Image

Container is made up of **images**. We have layers of stacked images. Mostly Linux base image (ie. alpine because of their small size)

Container running Postgresql:

- Layer - Linux base image
- Layer - Application image (ie. postgresql) -> Contains layers of compo

### Demo: Running PosgreSQL container

```shell
# command to run the container
> docker run --name postgres-container-1 -e POSTGRES_PASSWORD=mysecretpassword -d postgres
# we can specify a version by adding a tag after the image name, here 13.10
# docker run -e POSTGRES_PASSWORD=secret -d postgres:13.10

# if docker does not find the image locally, it will downloaded from container repo (by default dockerhub)
Unable to find image 'postgres:latest' locally

# image found in repo, so downloading/pulling
latest: Pulling from library/postgres

# Pulling all the layers being part of the postgres image
31ce7ceb6d44: Pull complete
28caef79f6ed: Pull complete
be24e278ddaa: Pull complete
dfdbb4d6f9a3: Pull complete
ebb06cfd45ec: Pull complete
9738a56db3f9: Pull complete
87cb08d2765c: Pull complete
d99960ffd545: Pull complete
5b7988269916: Pull complete
e5a82de43af8: Pull complete
af2aa815fc4c: Pull complete
66c1f54848f0: Pull complete
2d2755afc3bb: Pull complete

# additional information
Digest: sha256:a80d0c1b119cf3d6bab27f72782f16e47ab8534ced937fa813ec2ab26e1fd81e
Status: Downloaded newer image for postgres:latest
cfbc81bfc175a3afd185772a7fde9b111766458bf8f723c9e8b6e69c40f69c8e
```

Why spliting the image in layers? it allows to download only the layers we don't have when for example we have 2 images sharing the same layer (ie. 2 different versions of postgres), so, do not download the same layers twice.

    Image is the actual package, the artifact that can be moved around.
    Container is basically the application running from an image

## Docker (Container) vs Virtual Machine

### How an OS is made up

-> OS Application layer (applicaitons run here)
-> OS Kernel (communicate with the hardware)

Docker virtualize the application layer and use the kernel of the host beecause it doesn' have its own kernel.
VM has the application layer and its own kernel, so virtualize the complete OS.

### Main differences

- Size: Docker uses couple of MB vs couple of GB
- Speeed: Docker runs in seconds instead of minutes
- Compatibility: VM can run in any OS host

Docker was built to run natively in Lnux but needs Docker Desktop to run in Windows or MacOS.

### Docker Architecture and its components

When we install Docker, we install Docker Engine, who contains:

#### Server: Pulling images, managing images and containers

- Container runtime: pulling images, managing container lifecycle
- Volumes: Persisting data
- Network: Configuring network for container communication
- Build images: Build own Docker images

#### API: interacting with docker server

#### CLI: exectue docker commands

> Alternatives: containerd or cri-o (if need only a container runtime), buildah (if need to build an image)

## Main Docker commands

List running containers

```shell
# display the list of containers running
> docker ps
CONTAINER ID   IMAGE            COMMAND                  CREATED          STATUS          PORTS      NAMES
34df8bd93a26   postgres:13.10   "docker-entrypoint.s…"   25 minutes ago   Up 25 minutes   5432/tcp   postgres-container-2
cfbc81bfc175   postgres         "docker-entrypoint.s…"   33 minutes ago   Up 33 minutes   5432/tcp   postgres-container-1
```

List of the running and **stopped** containers

```shell
docker ps -a
```

Download an image

```shell
docker pull <image_name>[:<tag or version>]
```

Run a Docker container based on an image. It pulls the image and start the container, all in one command.

```shell
docker run -d <image_name>
```

-d will use the detached mode, this means after executing the command we can continue using the terminal and even if we close the terminal, the container will be still running. It works with <code>docker start</code> command as well.

Start a Docker container

```shell
docker start <container id>
```

Stop a Docker container

```shell
docker stop <contaner id>
```

List of downloaded images or images present in the machine

```shell
docker images
```

Delete container

```shell
docker rm <container ID or name>
```

Remove/delete Docker images

```shel
docker rmi <image name>
```

#### Container port vs Host port

Multiple containers can run on the host machine. We need to create a bind between host port and container port.
If the containers are using the same internal port, we need to bind a different Host port for each container port. Otherwise, we will get an error message: Port already in use

Example:

- Container1 port 3000 - Host port 3000
- Container2 port 3000 - Host port 3001

Binding port if made when running the container

```shell
docker run -p <host port>:<container port> -d <docker image>
```

Example: we can see in PORT column
redis1: host port 6000 bind to container port 6379
redis2: host port 6001 bind to container port 6379

```shell
> docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                    NAMES
fee1db999388   redis     "docker-entrypoint.s…"   2 seconds ago    Up 2 seconds    0.0.0.0:6000->6379/tcp   redis1
3ba733148c12   redis     "docker-entrypoint.s…"   11 seconds ago   Up 10 seconds   0.0.0.0:6001->6379/tcp   redis2
```

### Debug Commands

Check the logs

```shell
docker logs <container-ID or container-name>
```

Follow/stream the logs

add option _-f_ to get live logs

```shell
docker logs <container-ID or container-name> -f
```

Get inside the docker container

```shel
docker exec -it <container-ID or container-name> /bin/bash
```

This will give us access to open a bash session inside the container

## Docker network

Isolated docker network where the containers can run and communicate to each other without configuring any localhost or port, just because they are in the same network. The application (ie. node js app) running outsite of Docker (or from the host) will conect to the containers inside the network using localhost:port_number

If we run the app as a docker container and add it to the same docker network it won't need any additional configuration to talk to other containers inside the network. Then we can communicate to the app as usual (ie. localhost:3000)
Docker by default provide some networks

```
> docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
8975225a6625   bridge    bridge    local
e796ece0d423   host      host      local
2a4e77cb2129   none      null      local
```

### Create a docker network

```
docker network create <network name>
```

### Run a container in a specific docker network

```
docker run --network <network name> <image name>
```

Example of command using docker image for mongodb

```
$ docker run -d \
-p 27017:27017 \
--name mongodb \
--network mongo-network \
-e MONGO_INITDB_ROOT_USERNAME=admin \
-e MONGO_INITDB_ROOT_PASSWORD=password \
mongo
```

```
-p          port
-d          detached mode
--name      container name
--network   network name
-e          environment variables for container
```

## Develop with Containers

Simplified, but realistic workflow:

1. Developping a JS app (using docker image from dockerhub)
2. commit to git repo
3. this triggers a Jenkins CICD pipeline
4. which will push the built docker image to the private repository
5. then, the deployment server will pull:
   a. the docker image for our app (from private repository)
   b. and the mongo docker image needed (from dockerhub) to run our app.
6. Start our own application and mongo application using docker compose

!!! TODO: add picture showing workflow

★ **Demo Project: Developing with Docker**

We will show how Docker containers are actually used in the development process by running an app with the following components:

- Javascript application: Frontend Js + NodeJs backend
- Database: Docker container running a MongoDB image + Monogo express for a much easier way to make changes in the DB

See demo-projects/1-base/README.md file for specific details and command to run the demo project app.

## Run multiple containers - **Docker Compose**

Structure way to contain very normal docker commands. It simplifies container management by the usage of YAML files.

No need to specify a Docker Network, docker compose will create a common network for the containers declared in the docker compose file.

Docker compose file basic structure:

```yaml
# docker run -p 27017:27017 -d --network mongo-network --name=mongodb -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password mongo
version: "3"
services:
  # name of the container (equivalent to --name)
  mongodb:
    # image name
    image: mongo
    # list of ports host:container
    ports:
      - 27017:27017
    # list of environment variables
    environment:
      - MONGO_INITDB_ROOT_USERNAME=admin
      - MONGO_INITDB_ROOT_PASSWORD=password
```

### Usage of docker compose file

Start (up) the containers configured in our file

```shell
# -f  file
$ docker-compose -f <docker-compose.yaml file> up
```

Stop (down) the containers

```shell
$ docker-compose -f <docker-compose.yaml file> down
```

★ **Demo Project: Docker Compose - Running multiple services**

See demo-projects/2-using-docker-compose/mongo.yaml for a full example.

## Build own Docker Image - **Dockerfile**

Containerize an application using a base image (as a starting point), then adding our application package (with all the parameters needed) on top of it and finally, run the application. All these steps will be specified in our Dockerfile.

### Main commands

Base image

```
FROM node
```

\*node image is basicaly a linux image with node installed

Set the list of environment variables used by the app

```
ENV MONGO_DB_USERNAME=admin
ENV MONGO_DB_PWD=password
```

Execute any linux command from inside of the container
ie. /home/app directory will be created inside the container, not on the host

```
RUN mkdir -p /home/app
```

Copy current folder files (from the host) to /home/app (inside the container)

```
COPY . .home/app
```

Set a default working directory

```
WORKDIR /home/app
```

This will set a default working directory inside the container. So, when Docker container starts based on this image, all the following commands in the Dockerfile, after the WORKDIR command, will be executed in that _/home/app_ directory.

Entry point command

```
CMD [ "node", "server.js" ]
```

The differnce between RUN and CMD: only 1 CMD, but as many RUN as we want.

### Build a docker image from a Dockerfile

```
$ docker build -t my-app:1.0 .
```

-t image_name:tag
. location of Dockerfile

```
$ docker images
REPOSITORY      TAG       IMAGE ID       CREATED         SIZE
my-app          1.0       8b84026cd462   5 minutes ago   159MB
```

==When we adjust or make changes to the Dockerfile, we must rebuild the image.==

### Run the image

```
$ docker run image_name:tag
```

See what is inside the image

```
$ docker run -it image_name sh
```

Go and see inside of the container

```shell
$ docker logs container_id_or_name
...
# execute sh command inside the container
$ docker exec -it container_id_or_name /bin/sh
```

★ **Demo Project: Dockerfile - Building our own Docker Image**

See demo-projects/3-building-own-docker-image/Dockerfile for a full example.

## Push to private Docker repository

Steps required:

- Create Docker private repository
- Registry options
- Build & tag an image
- docker login
- docker push

==See **../AWS/ECR** folder for details how to create user and authentication to be able to push images to AWS ECR (Elastic Container Registry)==

#### Image Naming in Docker registries

registryDomain/imageName:tag

\*registryDomain = host, port ...

When pulling images from dockerhub, there is no need to specify the whole **registryDomain** which would be as follow for a mongo image for example:

docker pull mongo:4.2
docker pull docker.io/library/mongo:4.2

So by default, "docker pull" pulls images from Docker Hub
For private repositories we need to specify the full registryDomain, like in AWS ECR

★ **Demo Project: Private Docker Repository - Pushing our Docker**

See demo-projects/4-pushing-to-AWS-ECR/README.md for details about commands needed to push the docker image to AWD ECR

## Deploy containerized application

### Image into a private Registry on AWS

★ **Demo Project: Deploying our containerized application**

Changes in the app:

- Add my-app image details in docker compose file like so we can start all the 3 containers at once.

See demo-projects/5-deploying-containerized-application/my-app-and-mongo.yaml for a full example.

## Persist data in Docker - **Volumes**

Docker Volumes are used for data persistence applications like databases or other stateful applications.

- When do we need docker volumes
  So, needed when we don't want to lose our data when the container is removed or restarted.

- What is a docker volume
  Folder in physical host file system is **mounted** into the virtual file system of Docker
  Data get replicated automatically from container to host and visceversa

- 3 Volume Types
  a. Host volumes: We decide **where on the host file system** the reference is made

  ```
  # host directory:container directory
  $ docker run -v /home/mount/data:/var/lib/mysql/data ...
  ```

  b. Anonymous volumes: For each container a folder is generated that gets mounted

  ```
  # just the container directory
  $ docker run -v /var/lib/mysql/data ...
  ```

  c. Named volumes: Like anonymous volumes but we can reference the volume by name.

  ```
  # name/reference + the container directory
  $ docker run -v name:/var/lib/mysql/data ...
  ```

  Named volumes **should be used in production** to let Docker manages those volumes directories on the host.

  See where the Docker Volumes are located for Named Volumes:

  - Windows: C:\ProgramData\docker\volumes
  - /var/lib/docker/volumes
  - /var/lib/docker/volumes

- Docker Volumes in docker-compose file

```
version: "3"

services:
  mongodb:
    image: mongo
    ports:
      - 27017:27017
    # define a volume
    volumes:
      - db-data:/var/lib/data

# list of volumes defined previously
volumes:
  db-data
```

★ **Demo Project: Volumes - Configuring persistence for our application**

Changes in the app:

- new section added in docker compose file to define volumes for mongo container

Location used by default to store data by databases

- mongodb /data/db
- mysql /var/lib/mysql
- postgres /var/lib/postgresql/data

See demo-projects/6-using-Volumes-to-persist-app-data/my-app-and-mongo.yaml for an example of how integrate volumes in docker compose file and README.md for details about runnning the app

## Nexus & Docker

### Create private Docker Repository on Nexus

#### Create Docker repository in Nexus

-> Go to "Server administration and configuration" / Repositories / create Repository / docker hosted / set name / select blob store / Create repository button
get the repository url. ie http://206.189.124.8:8081/repository/docker-hosted/
untick option "If checked, the repository accepts incoming requests"

#### Create user role for Docker repo

-> Administration section / Security / Roles / Create Role / Nexus Role
-> Role ID, Role Name and Privileges
-- in Privileges look for 'docker' (admin for admin, view for users), select nx-repository-view-docker-docker-hosted-\*
-> Save

#### Assign the role to an user

-> Security / Users / select user / select role

#### Docker Login to Nexus docker repo

-> Repositories / select repo / Edit HTTP field (because we are using a not secured connection, otherwise HTTPS would be)
Docker client cannot connect to a path that represent a nexus repo like <code>http://206.189.124.8:8081/repository/docker-hosted/</code>
So, we need to set a port for docker repo specifically. ie 8083

url to use when pushing to docker repo: http://206.189.124.8:8083

On server where nexus is running:

```
$ netstat -lnpt
```

```
tcp        0      0 0.0.0.0:8083            0.0.0.0:*               LISTEN      4242/java
tcp        0      0 0.0.0.0:8081            0.0.0.0:*               LISTEN      4242/java
...
```

to check the port (8083) is opened
Add this port to the firewall rules to be able access it.

##### Get token for authentication from Nexus

When using <code>docker login</code> we get a token of authentication on Nexus docker repository for a client. The token will be stored in ~/.docker/config.json
Docker config file contains the configuration for docker but also all the tokens/authentications made to all the different docker repositories.

Go to Realms to acivate the token:
-> Security / Realms / select Docker Beare Token Realm to move it to 'Active' section to activate the

==Working with insecure repositories==
Docker allows, by default, client requests whenever we use docker commands (logn, pull, push) to go to and talk to HTTPS endpoints (secure endpoints) of the docker registry.

Since we don't have a secure registry (no https configured), we need to tell docker to accept http requests for a not secure repo. docs.docker.com/registry/insecure

### Authenticate, Pull and Push to Nexus Repository

See commands and details in demo-projects/7-pushing-to-Nexus/README.md

### Run Nexus as Docker container

Set of commands to run in server

```
$ apt update
$ apt install docker.io
$ docker pull sonatype/nexus
```

Check dockerhub documentation for up-to-date details how to run nexus image (https://hub.docker.com/r/sonatype/nexus), Persistant Data section

```
$ docker run -d --name nexus-data sonatype/nexus echo "data-only container for Nexus"
$ docker run -d -p 8081:8081 --name nexus --volumes-from nexus-data sonatype/nexus
```

## Docker Best Practices

see Best-Practices.md
