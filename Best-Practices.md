1. Use official docker images as Base Image whenever available.
   If an official image exists, use it instead of creating one from scratch.
   ie. use node docker image instead of using linux as base image and install docker on it.

```
👎 FROM ubuntu
RUN apt-get update & apt-get install -y \
    node \
    ...
vs
👍 FROM node
```

2. Use specific image version, instead of latest. The more specific the better.

```
👎 FROM node
vs
👍 FROM node:20.0.2
even better
👍👍 FROM node:20.0.2-alpine
```

3. Use small images base on leaner operating system distribution like alpine instead of specific linux version based like ubuntu.
   -> avoid large image size, security vulnerabilities

4. Optimize caching image layers by ordering Dockerfile commands from least to most frequently changing and take advantage of caching for faster image building and fetching
   -> Each line in a docker file will add a layer in the construction of the final docker image on top of the image used as base for our image
   -> Display the layer and corresponding commmands that create the layer

```
docker history <image_name>:<tag>
```

-> Faster image building
-> Downloading only added layers
-> Use an optimized position for each command the Dockerfile
Once a layer changes the other following layers need to be recreated again. Like here anytime there is a change in _myapp_ folder, the COPY command is executed so RUN will be as well.

```
FROM node:20-alpine
WORKDIR /app
COPY myapp /app
RUN npm install
CMD [ "node", "server.js" ]
```

This is an inconvinient because <code>RUN npm install</code> will re-install all the dependencies every time instead of using caching.
So, we can optimize caching by adding a COPY only for dependencies (package.json) before RUN and after this the COPY for myapp

```
FROM node:20-alpine
WORKDIR /app
COPY package.json package-lock.json .
RUN npm install
COPY myapp /app
CMD [ "node", "server.js" ]
```

This way, the dependencies will be re-install only if the list of dependencies changes. If any other file changes in the project, the RUN layer will be re-used from cache.

5. Use .dockerignore file to explicitly exclude files and folders
   -> located in app root folder
   -> List files and folders we want to ignore

6. Make use of "Multi-Stage builds": Use multiple temporary images during the build process but keep only the latest image as the final artifact.
   Content needed for building the image BUT no needed in the final image to run the app: Test dependencies, development tools, temporary files, build tools. ie: JDK needed to compile Java code but not needed to run the Java app itself, Maven/Gradle to build the app but not to run it.

```
FROM apache
RUN apt-get update & apt-get -y install maven
WORKDIR /app
COPY myapp /app
RUN mvn package
COPY /app/target/file.war /usr/local/tomcat/webapps
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/usr/local/lib./demo.jar"]
```

optimized:

```
# Build stage named 'build' here
FROM maven AS build
WORKDIR /app
COPY myapp /app
RUN mvn package

# Run stage
FROM tomcat
COPY --from=build /app/target/file.war /usr/local/tomcat/webapps
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/usr/local/lib./demo.jar"]
```

-> use AS to name a stage
-> each FROM starts a new build stage
-> we can copy artifcats from one stage to another (COPY --from=build)
-> the final application image is created only in the last stage
-> all the files and tools used in previous stages will be discarded once they're completed

7. Use the least privileged user by creating a dedicated user and group to run the application. Do not forget to set required permissions
   -> When a Dockerfile do not specify a user, it uses root user (security issue)

```shell
...
# create group and user
RUN groupadd -r tom && useradd -g tom tom
# set ownership and permissions
RUN chown -R tom:tom /app
# switch to non-root user
USER tom
CMD node index.js
```

Conveniently, some images already have a generic user, like node image use user node

8. Scan our images for security vulnerabilities
   -> we nned to be logged into Docker Hub to be able to scan the images

```shell
$ docker login
..
$ docker scout cves <image-name>:<tag>
```

docker will run a scan using docker known vulnerabilities database
