## demo app - Pushing to AWS ECR (private container repository)

### To push the image (given as example)

#### Authenticate Docker client to Registry

We need to create user and credentials first to be able to execute this command. See **AWS/ECR** (in root folder) for details

    aws ecr get-login-password --region eu-north-1 | docker login --username AWS --password-stdin 562447325073.dkr.ecr.eu-north-1.amazonaws.com

#### Build the image

    docker build -t devops-bootcamp-sample-app:1.0 .

#### Create a tag

    docker tag devops-bootcamp-sample-app:1.0 562447325073.dkr.ecr.eu-north-1.amazonaws.com/devops-bootcamp-sample-app:1.0

#### Push to the repository

    docker push 562447325073.dkr.ecr.eu-north-1.amazonaws.com/devops-bootcamp-sample-app:1.0

### To start the application

Step 1: start mongodb and mongo-express

    docker-compose -f mongo.yaml up

_You can access the mongo-express under localhost:8081 from your browser_

Step 2: in mongo-express UI - create a new database "user-account"

Step 3: in mongo-express UI - create a new collection "users" in the database "user-account"

Step 4: start node server

    cd app
    npm install
    node server.js

Step 5: access the nodejs application from browser

    http://localhost:3000
