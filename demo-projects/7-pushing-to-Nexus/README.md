## demo app - Deploying containerized application

This demo app shows a simple user profile app set up using

- index.html with pure js and css styles
- nodejs backend with express module
- mongodb for data storage

All components are docker-based

### To push the image (given as example)

#### Authenticate Docker client to Registry

    docker login <nexus ip address>:<port>

ie. docker login 206.189.124.8:8083

#### Build the image

    docker build -t my-app:1.0 .

#### Create a tag

create tag including the url and port of our nexus repository

    docker tag my-app:1.0 206.189.124.8:8083/my-app:1.0

#### Push to the repository

    docker push my-app:1.0 206.189.124.8:8083/my-app:1.0

### To start the application

Step 1: start mongodb and mongo-express

    docker-compose -f my-app-and-mongo.yaml up

_You can access the mongo-express under localhost:8081 from your browser_

Step 2: in mongo-express UI - create a new database "user-account"

Step 3: in mongo-express UI - create a new collection "users" in the database "user-account"

Step 4: start node server

    cd app
    npm install
    node server.js

Step 5: access the nodejs application from browser

    http://localhost:3000
