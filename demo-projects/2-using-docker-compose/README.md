## demo app - using docker-compose for mongo and mongo-express

### To start the application

Step 1: start mongodb and mongo-express

    docker-compose -f mongo.yaml up

_You can access the mongo-express under localhost:8081 from your browser_

Step 2: in mongo-express UI - create a new database "user-account"

Step 3: in mongo-express UI - create a new collection "users" in the database "user-account"

Step 4: start node server

    cd app
    npm install
    node server.js

Step 5: access the nodejs application from browser

    http://localhost:3000
