# Push image to dockerhub

## Build a docker image

```
$ docker build -t my-app:1.0 .
```

When using Mac to build images

```
$ docker build --platform linux/amd64 -t my-app:1.0 .
```

## Push to Docker repository

Steps required:

- Create Docker repository

  name repo = image name

- Create tag

  $ docker tag local-image:tagname YOUR-USER-NAME/new-repo:tagname

- docker login

  $ docker login -u YOUR-USER-NAME

- docker push

  $ docker push YOUR-USER-NAME/new-repo:tagname

## Pull the image

```
$ docker login -u YOUR-USER-NAME
$ docker pull YOUR-USER-NAME/repo-name:tagname
```

## Run container

```
$ docker run -d -p <host>:<container> YOUR-USER-NAME/repo-name:tagname
```

To remove warning message similar to:
==WARNING: The requested image's platform (linux/arm64) does not match the detected host platform (linux/amd64/v3) and no specific platform was requested==

```
$ docker run -d --platform linux/amd64 -p <host>:<container> YOUR-USER-NAME/repo-name:tagname
```
